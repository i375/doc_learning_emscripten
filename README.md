# Learning Emscripten (and using WebAssembly) #

## Where to start and create `hello world`?

After installing Emscripten SDK. By following instruction [here](http://kripken.github.io/emscripten-site/docs/getting_started/downloads.html).

In bash:
```Bash
mkdir learning_emscripten
cd learning_emscripten
touch main.c
```

Then enter text in `main.c`:
```c
// main.c

#include <stdio.h>

int main(){
  printf("Hello world\n");
}
```

To compile execute (in bash)
```bash
emcc main.c
```

And then to execute:
```bash
node a.out.js
```

## How to build for web browser?  
For generating files runnable in browser file shell be built by specifying output file:  
```bash
emcc main.c -o index.html
```
This command will generate `index.html` and accompaning `JavaScript` files.  
Running example can be viewed by running `http` server in the folder where `index.html` resides and opening that address in browser
or by just running following command `emrun index.html`, this will start `http` server and open `index.html` in browser.

## Is there a way to make code perform faster and also minimize generated `JavaScript`?  
Yes, files can be compiled to [`WebAssembly`](http://webassembly.org/) and also optimizations applied to them by compiling with `-s WASM=1 -O3` flags specified:
```bash
emcc -s WASM=1 -O3 main.c -o index.html
# and then run
emrun index.html
```
`-s WASM=1` tells compiler to generate `WebAssembly` code and `-O3` tells compiler to optimize generated code.

## How can I get basic `string` input from user?

It can be done with standard `libc` function [`fgets`](http://en.cppreference.com/w/c/io/fgets), which reads string (or part of full string) up to the number of symbols specified with second parameter, in our case file is `stdin` standard input.  
Particular line of code doing thie is `fgets(buffer, BUFFER_SIZE, stdin);` and full program:
```c
// main.c

#include <stdio.h>

int main(){
  const int BUFFER_SIZE = 17;
  char buffer[BUFFER_SIZE];
  printf("Please enter some text: ");
  fgets(buffer, BUFFER_SIZE, stdin);
  printf("String you entered: %s", buffer);  
}
```
when `fgets(buffer, BUFFER_SIZE, stdin);` gets executed `Javascript` `prompt` appears. For some reason after entering text in `prompt` and pushing `OK` buttom it keeps reappearing
but that's not critical for our purposes as it's anyway better to implement web inputs using `DOM` registering event handlers.
But for overcoming current problem just push `OK` at first then push `Cancel` and code will get it's inputs. 

## How can I run infinite game loop and integrate with Browser's `requestAnimationFrame`

For running inifinite game loop, one must set function to be called infinitelly. That can be done by calling
[`emscripten_set_main_loop(inifinite_loop_function_pointer, frames_per_second, simulate_infinite_loop)`](https://kripken.github.io/emscripten-site/docs/api_reference/emscripten.h.html#c.emscripten_set_main_loop), where first
parameter `inifinit_loop_function_pointer` is pointer to function which we want to be called infinitelly, second
parameter `frames_per_second` is number of times we want our function to be called per second (**note:** if set to `0` then
`requestAnimationFrame` gets used which is good, otherwise `setTimeout` is used to match our desired FPS), and the last parameter
is kind of weird but if `true` throws exception to stop caller execution and if `false` just sets up infinite loop
and doesn't do additional "damage".

```c
// main.c

#include <stdio.h>
#include <emscripten.h>

void main_loop();

int main(){
  emscripten_set_main_loop(main_loop, 0, 0);
}

void main_loop(){
  static int frameCounter = 0;

  printf("Frame: %d\n", frameCounter++);
}
```

## Now, how can I interact with `DOM` and `JavaScript`?

Fist, for interacting (calling) with `JavaScript` macro `EM_ASM` can be used. Say for creating `div` element 
for outputing text with, instead of using `printf` `libc` function.  
**note:** `JavaScript` lines inside `EM_ASM` macro shell end with `;` for them to compile properly,
at least in my experience. 

Code for creating the `div` looks like (creates `div` with border and reddish background):
```c
// part of main.c

  EM_ASM(
    var debugDiv = document.createElement('div');
    document.body.append(debugDiv);
    window.debugDiv = debugDiv;
    debugDiv.style.border = "solid 1px black";
    debugDiv.style.backgroundColor = "#ffeeee";
    debugDiv.innerHTML = "Debug div";
  );
```
_node: adding fields to `window` object is bad idea, some other place shell be used in production_

As a second step I need mechanism for passing data to `JavaScript` inside `EM_ASM` macro. Say `string` to
be assigned to `debugDiv.innerHTML`. For that task macro `EM_ASM_` can be used, which takes `JavaScript` block,
followed with parameters (`int`s or `double`s), parameters can be accessed by `$0`, `$1`,... inside `JavaScript`.

Code for that looks like:
```c
  EM_ASM_({
    window.debugDiv.innerHTML = Module.UTF8ToString($0);
  }, debug_text);
```
See macro `EM_ASM_` takes `JavaScript` code block and currently just one parameter `debug_text`
that's just `C` variable, namely pointer to `char` array, `char*`. To create `JavaScript` `string` from that pointer
I use function [`Module.UTF8ToString`](https://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html#UTF8ToString) inside `JavaScript` and pass it first parameter `$0`
 which holds value of `debug_text`. **note:** to create `C` string from `JavaScript` string [`Module.stringToUTF8`](https://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html#stringToUTF8) can be used.  

But there's one more thing to do, to use `UTF8ToString`. I need to specify in compilation command that I need
that function to be exported for use. As compilation command get's little bit complicated I've created
`build.sh` file and put compilation command inside:
```bash
# build.sh
emcc -s WASM=1  \
-s EXPORTED_FUNCTIONS="['_main']" \
-s EXTRA_EXPORTED_RUNTIME_METHODS="['UTF8ToString', 'stringToUTF8']" \
-O3 main.c -o index.html
```

And full listing of code, which creates `div` and modifies `innerHTML` from `C` code:
```C
// main.c

#include <stdio.h>
#include <emscripten.h>

void main_loop();
void create_debug_div();
void set_debug_text(char *debug_text);

int main(){
  create_debug_div();
  emscripten_set_main_loop(main_loop, 60, 0);
}

void main_loop(){
  static int frame_counter = 0;
  char message[64] = {0}; // Create buffer for holding message string

  sprintf(message, "%s %d", "Frame: ", frame_counter);
  set_debug_text(message);
  frame_counter++;
}

void create_debug_div(){
  EM_ASM(
    var debugDiv = document.createElement('div');
    document.body.append(debugDiv);
    window.debugDiv = debugDiv;
    debugDiv.style.border = "solid 1px black";
    debugDiv.style.backgroundColor = "#ffeeee";
    debugDiv.innerHTML = "Debug div";
  );
}

void set_debug_text(char *debug_text){
  EM_ASM_({
    window.debugDiv.innerHTML = Module.UTF8ToString($0);
  }, debug_text);
}
```
or make them `inline` and trust compiler.

Now after I learned how to execute `JavaScript` and pass data to it.

## How do I return data from `JavaScript` to `C`, called from `C`?

Macro `EM_ASM` supports returning `int` or `double`. Well ability of returning `double` might be and certainly is usefull thing but ability
of returning `int`s is quite powerfull, because by returning `int`, the data returned can be some pointer to memory
allocated withing `JavaScript` code which holds say some `C` `struct` of data.

But becofe getting to far away, here's simple example of returning `int` from `JavaScript` code withing `EM_ASM` macro.
Say, generating random number in `JavaScript` and getting it back in `C`:
```C
// part of main.c

EM_ASM_INT({
    return $0 + Math.floor(Math.random() * ( $1 - $0 ));
  },min, max);
  
// or wrapped in C function
int generate_random_number(int min, int max)
{
  return EM_ASM_INT({
    return $0 + Math.floor(Math.random() * ( $1 - $0 ));
  },min, max);
}
```

Another way of **returning/passing data from `JavaScript` to `C` is through pointer(s) passed in as argument(s) to `EM_ASM_` macro**. 
So what if I want to generate array of random numbers and return it from `JavaScript`,
ways to do that are various, but for my purposes I'll pass in `JavaScript`
pointer to array of `int`s and write generated random numbers back in to `C` memory. 
I used [`Module.setValue(pointerToValue, valueToWrite, valueType)`](https://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html#setValue) function for writing `C` memory from `JavaScript`:
```c
// part of main.c

enum keyboard_event_type
{
  KEY_DOWN = 1,
  KEY_UP = 0
};

void generate_random_numbers(int* arr_pointer, int bytes_per_number, int count, int min, int max) {
  EM_ASM_({
    for(var i = 0; i < $2; i++) {
      var rndNumber = $3 + Math.floor(Math.random() * ( $4 - $3 ));
      Module.setValue($0 + i * $1, rndNumber, 'i32');
    }
  },arr_pointer, bytes_per_number, count, min, max);
  //$0           $1                $2     $3   $4
}
```
Also `build.sh` gets an update:
```bash
# build.sh

emcc -s WASM=1  \
-s EXPORTED_FUNCTIONS="['_main']" \
-s EXTRA_EXPORTED_RUNTIME_METHODS="['UTF8ToString', 'stringToUTF8', 'setValue', 'getValue']" \
-O3 main.c -o index.html

# -s EXPORTED_FUNCTIONS  Specifies function to be exported and kept so (during optimizations) from C part
# -s EXTRA_EXPORTED_RUNTIME_METHODS  Specifies functions to be exported and kept so (during optimizations) from JavaScript part
```

So for calls initiated from `C` to `JavaScript` this answer all my basic needs. And now I need to know

## How to call `C` from `JavaScript` and pass data.

I'll imagine scenario where I have some `JavaScript` library `mylib.js` which sets up keyboard event handlers and redirects
events to `C` code by calling `handle_keyboard_event(keyboard_event_type, key_code)` function defined in `C` code, which looks like:
```C
// Part of main.c

enum keyboard_event_type
{
  KEY_DOWN = 1,
  KEY_UP = 0
};

// ...

void handle_keyboard_event(enum keyboard_event_type ev_type, int key_code){
  if(ev_type == KEY_UP){
    printf("Key %d is UP\n", key_code);
  } else if(ev_type == KEY_DOWN) {
    printf("Key %d is DOWN\n", key_code);
  }
}
```

To call `handle_keyboard_event` from `JavaScript` I use method proveded by `Emscripten`
library [`Module.ccall`](https://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html#ccall)
use of which looks like for this particular case:
```JavaScript
// Part of mylib.js

//           Function to call. 
//             |               Return type
//             |                      |
//             |                      |     Parameter types       Parameters
//             ↓                      ↓     ↓                     ↓
Module.ccall("handle_keyboard_event", null, ['number', 'number'], [1, e.keyCode])
```
Or I can create `JavaScript` wrapper for
the function with [`Module.cwrap`](https://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html#cwrap),
which can be reused throughout the code:
```JavaScript
// Part of mylib.js

var handle_keyboard_event = Module.cwrap("handle_keyboard_event", null, ['number', 'number'])

//...

handle_keyboard_event(0, e.keyCode) // Actual call
```

Full `mylib.js` looks like it simply registers event handlers and redirects them to `C` code:
```JavaScript
// mylib.js

(function () {
    function main() {
        var handle_keyboard_event = Module.cwrap("handle_keyboard_event", null, ['number', 'number'])

        window.addEventListener("keydown", function (e) {
            Module.ccall("handle_keyboard_event", null, ['number', 'number'], [1, e.keyCode])
        })

        window.addEventListener("keyup", function (e) {
            handle_keyboard_event(0, e.keyCode)
        })
    }

    window.addEventListener("load", function (e) {
        main()
    })
})()
```
Now, for me to be able to call `handle_keyboard_event` with `Module.ccall` and `Module.cwrap`, and also to include `mylib.js` in build
I add these functions
to be exported and kept so (during optimizations) in `build.sh` and also specify `mylib.js` with `--post-js mylib.js`
to be included in build:
```bash
# build.sh

emcc -s WASM=1  \
-s EXPORTED_FUNCTIONS="['_main', '_handle_keyboard_event']" \
-s EXTRA_EXPORTED_RUNTIME_METHODS="['UTF8ToString', 'stringToUTF8', 'setValue', 'getValue', 'ccall', 'cwrap']" \
--post-js mylib.js \
-O3 main.c -o index.html

# -s EXPORTED_FUNCTIONS  Specifies function to be exported and kept so (during optimizations) from C part
# -s EXTRA_EXPORTED_RUNTIME_METHODS  Specifies functions to be exported and kept so (during optimizations) from JavaScript part
```

Also full `C` code at this point looks like this:
```C
// main.c

#include <stdio.h>
#include <emscripten.h>
#include <stdint.h>

enum keyboard_event_type
{
  KEY_DOWN = 1,
  KEY_UP = 0
};

void main_loop();
void create_debug_div();
void set_debug_text(char *debug_text);
int generate_random_number(int min, int max);
void generate_random_numbers(int* arr_pointer, int bytes_per_number, int count, int min, int max);
void handle_keyboard_event(enum keyboard_event_type ev_type, int key_code);

int main(){
  create_debug_div();
  emscripten_set_main_loop(main_loop, 60, 0);
}

void main_loop(){
  static int frame_counter = 0;
  char message[64] = {0}; // Create buffer for holding message string

  int rnd_number = generate_random_number(0,100);

  int32_t randoms[2];
  generate_random_numbers(randoms, sizeof(int32_t), 2, 0, 100);

  sprintf(message, "Frame: %d<br/> Random: %d<br/> Randoms: %d, %d", frame_counter, rnd_number, randoms[0], randoms[1]);
  set_debug_text(message);

  frame_counter++;
}

void handle_keyboard_event(enum keyboard_event_type ev_type, int key_code){
  if(ev_type == KEY_UP){
    printf("Key %d is UP\n", key_code);
  } else if(ev_type == KEY_DOWN) {
    printf("Key %d is DOWN\n", key_code);
  }
}

void generate_random_numbers(int* arr_pointer, int bytes_per_number, int count, int min, int max) {
  EM_ASM_({
    for(var i = 0; i < $2; i++) {
      var rndNumber = $3 + Math.floor(Math.random() * ( $4 - $3 ));
      Module.setValue($0 + i * $1, rndNumber, 'i32');
    }
  },arr_pointer, bytes_per_number, count, min, max);
  //$0           $1                $2     $3   $4
}

int generate_random_number(int min, int max) {
  return EM_ASM_INT({
    return $0 + Math.floor(Math.random() * ( $1 - $0 ));
  },min, max);
}

void create_debug_div(){
  EM_ASM(
    var debugDiv = document.createElement('div');
    document.body.append(debugDiv);
    window.debugDiv = debugDiv;
    debugDiv.style.border = "solid 1px black";
    debugDiv.style.backgroundColor = "#ffeeee";
    debugDiv.innerHTML = "Debug div";
  );
}

void set_debug_text(char *debug_text){
  EM_ASM_({
    window.debugDiv.innerHTML = Module.UTF8ToString($0);
  }, debug_text);
}
```

## Next I want to know, How to customize html theme of generated build?

Turns out all I have to do is specify `.js` as output file in compilation command like `emcc -O3 main.c -o index.js` (instead of `emcc -O3 main.c -o index.html`)
then include this `index.js` in my custom `html` file and do whatever I'd like. 

## To be continued

***  

# Additional references

- [Interperating between javascript and c code](https://kripken.github.io/emscripten-site/docs/porting/connecting_cpp_and_javascript/Interacting-with-code.html)
- [Browser main loop integration](https://kripken.github.io/emscripten-site/docs/porting/emscripten-runtime-environment.html#browser-main-loop)
- [Passing string from JavaScript to C, stringToUTF8](https://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html#stringToUTF8)
- [Passing string from C to JavaScript, UTF8ToString](https://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html#UTF8ToString)
- [emcc command documentation](https://kripken.github.io/emscripten-site/docs/tools_reference/emcc.html)